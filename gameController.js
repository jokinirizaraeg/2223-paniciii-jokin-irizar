
import { getAllHeroes, getJunkpile, getRandomHero } from "./gameService.js";



function Erudito(name, ang,  hpg) {
    this.name = name;
    this.ang = ang;
    this.hpw = 1+this.ang;
    this.hpg = hpg;
  }
  const elErudito = new Erudito("El erudito X.G.", 0,  "invincible");

export async function calcIniciative(hero, junkpile) {
  let heroIniciativeValue =
    hero.powerstats.intelligence + hero.powerstats.combat;
  let junkpileIniciativeValue =
    junkpile.powerstats.intelligence + junkpile.powerstats.combat;
  let iniciative;

  if (heroIniciativeValue > junkpileIniciativeValue) {
    iniciative = true;
  } else {
    iniciative = false;
  }

  return iniciative;
}

export async function calcHP(hero) {
  let heroHP = hero.powerstats.strength * 10;

  if (heroHP > 666) {
    heroHP = 666;
  }
  hero.hp = Math.floor(heroHP) ;

  return hero;
}

export async function combat(heroIniciative, hero, junkpile) {
    let eruditeActive = false
    let changeGlasses = false
    let eruditeAlive = true
    let eruditeTurns = Math.floor(Math.random() * (7 - 3 + 1) + 3)
    let activeHero
    let D100
    let D20
    let D20Erudito
    let D3
    let damage = 0
    let success = false
    let count = 0
    hero.leftArmDamaged=false
    hero.rightArmDamaged=false
    junkpile.leftArmDamaged=false
    junkpile.rightArmDamaged=false
    console.log("------------------------------------------------------------")
    
    console.log("Junkpile es retado por " + hero.name)
   
    console.log("------------------------------------------------------------")
  do {
    count++
    console.log("------------------------------------------------------------")
    console.log( hero.name)
    console.log(hero.powerstats)
    console.log("HP:" + hero.hp)
    console.log("------------------------------------------------------------")
    console.log(junkpile.name)
    console.log(junkpile.powerstats)
    console.log("HP:" + junkpile.hp)
    console.log("------------------------------------------------------------")
    console.log("Asalto número "+count)

    console.log("------------------------------------------------------------")
    if (heroIniciative){
        activeHero = hero
        console.log(heroIniciative)
        console.log("El asalto es para "+ hero.name)
    } else{
        activeHero = junkpile
        console.log("El asalto es para "+ junkpile.name)
    }

    if(!changeGlasses){
    if(count%eruditeTurns==0&&eruditeAlive){
      eruditeActive = true
    }else{
      eruditeActive = false
    }
  }
    if (eruditeActive){
      let res = eruditeTurn(activeHero)
      changeGlasses = false
      eruditeAlive = res[0]
      changeGlasses = res[1]
      heroIniciative = !heroIniciative


     
    }
    else{
    D100 = rollDice(100)
    if (D100 <= activeHero.powerstats.combat){
        
        console.log(activeHero.name+ " obtiene un "+ D100+ " y acierta")
        success=true
    }
    else{
        console.log(activeHero.name+ " obtiene un "+ D100+ " y falla")
        success=false
    }

    if (success){
        D20 = rollDice(20)
        console.log(activeHero.name+ " saca un "+ D20 + "en la tirada de ataque")
        if (D20>17){
            damage = criticalHit(activeHero,D20)
            if (heroIniciative){
                console.log("CRITICAL HIT!!")
                junkpile.hp = junkpile.hp-damage
                console.log(activeHero.name + " consigue hacer " + damage + " puntos de daño a su oponente")

                heroIniciative = false
            }
            else{
                console.log("CRITICAL HIT!!")
                hero.hp = hero.hp - damage
                console.log(activeHero.name + " consigue hacer " + damage + " puntos de daño a su oponente")
                heroIniciative = true
            }
        }else if(D20>2){
            damage = normalHit(activeHero,D20)
        if (heroIniciative){
            junkpile.hp = junkpile.hp-damage
            console.log(activeHero.name + " consigue hacer " + damage + " puntos de daño a su oponente")
            heroIniciative = false
        }
        else{
            hero.hp = hero.hp - damage
            console.log(activeHero.name + " consigue hacer " + damage + " puntos de daño a su oponente")
            heroIniciative = true
        }
        }else{
            fumble(activeHero,D20)
            if (heroIniciative){
                console.log("FAIL!!")
                hero.hp = hero.hp-damage
                console.log(activeHero.name + "pifia y se hace " + damage + " puntos de daño a si mismo")
                heroIniciative = false
            }
            else{
                console.log("FAIL!!")
                junkpile.hp = junkpile.hp - damage
                console.log(activeHero.name + "pifia y se hace " + damage + " puntos de daño a si mismo")
                heroIniciative = true
            }
        }


    }
    else{
        heroIniciative = !heroIniciative
    }
    
  }
  } while (hero.hp > 0 && junkpile.hp > 0);
  function Person(first, last, age, eye) {
      this.firstName = first;
      this.lastName = last;
      this.age = age;
      this.eyeColor = eye;
    }
    console.log(hero.name)
    console.log(hero.powerstats)
  console.log("HP:" + hero.hp)
  console.log("------------------------------------------------------------")
  console.log(junkpile.name)
  console.log(junkpile.powerstats)
  console.log("HP:" + junkpile.hp)
  console.log("------------------------------------------------------------")
  console.log("Asalto número "+count)
    console.log("------------------------------------------------------------")

  if (hero.hp<0){
    console.log(hero.name+ " Ha sido derrotado")
  }else{
    console.log(junkpile.name + " Ha sido derrotado")
  }

  return hero;
}



function rollDice (sides) {

    let diceRoll = Math.floor(Math.random() * ((sides-1) - 1 + 1)) + 1;

    return diceRoll

}

function normalHit (hero,D20) {
let damage= 0
 damage = (((hero.powerstats.power+hero.powerstats.strength)*D20)/100)
 damage = Math.ceil(damage)
 return damage
}

function fumble(hero,D20) {
    let damage = 0
    let D3 = 0
    let D3sum = 0
     if(D20==1){
        for (let i = 1; i < 5; i++) {
            D3 = rollDice(3)
            D3sum = D3sum+D3
          }
        damage=(hero.powerstats.speed/D3sum)
     }else{
        D3 = rollDice(3)
        D3sum = D3
        damage=(hero.powerstats.speed/D3sum)
     }
     damage = Math.ceil(damage)
     damage = Math.floor(damage)
     return damage
    }

function criticalHit (hero,D20) {

    let damage = 0
    let D3 = 0
    let D5 = 0
    let D5sum = 0
    let D3sum = 0

    if (D20==18){
        D3 = rollDice(3)
        D3sum = D3

        damage = (((hero.powerstats.intelligence*hero.powerstats.durability)/100)*D3sum)

    } else if(D20==19){


        for (let i = 1; i < 3; i++) {
            D3 = rollDice(3)
            D3sum = D3sum+D3
          }
          

        damage = (((hero.powerstats.intelligence*hero.powerstats.durability)/100)*D3sum)


    }
    else{

        for (let i = 1; i < 4; i++) {
            D5 = rollDice(5)
            D5sum = D3sum+D3
          }
          

        damage = (((hero.powerstats.intelligence*hero.powerstats.durability)/100)*D5sum)

    }
    damage = Math.ceil(damage)
    return damage

}


function eruditeTurn (activeHero) {
  let damage = 0
  let changeGlasses = false
  let updatedStrength = 0
  let eruditeAlive = true
  let D20Erudito
  console.log("EL ERUDITO APARECE")
  D20Erudito = rollDice(20)

  elErudito.ang = D20Erudito
  elErudito.hpw = elErudito.ang+1
  console.log("La ira del erudito es de "+ elErudito.ang)

  if (elErudito.ang<4){
    if(!activeHero.leftArmDamaged){
      damage = rollDice(20)
      updatedStrength = (activeHero.powerstats.strength/2)
      updatedStrength = Math.floor(updatedStrength)

      activeHero.powerstats.strength= updatedStrength
      activeHero.hp = (activeHero.hp-damage)

      console.log("Por culpa de las gafas del erudito " + activeHero.name + " se daña la mano izquierda y sufre "+ damage + " puntos de daño")
    }
    else{
      console.log("Por suerte o por desgracia " +  activeHero.name + " ya tenía la mano izquierda dañada, nada sucede")
    }
  }else if (elErudito.ang<7){
    if(!activeHero.rightArmDamaged){
      damage = rollDice(20)
      updatedStrength = (activeHero.powerstats.strength/2)
      updatedStrength = Math.floor(updatedStrength)

      activeHero.powerstats.strength= updatedStrength
      activeHero.hp = (activeHero.hp-damage)
      console.log("Por culpa de las gafas del erudito " + activeHero.name + " se daña la mano derecha y sufre "+ damage + " puntos de daño")
    }
    else{
      console.log("Por suerte o por desgracia " +  activeHero.name + " ya tenía la mano derecha dañada, nada sucede")
    }

  }else if (elErudito.ang<10){
    console.log(activeHero.name  + " no pudo con el caos de las gafas y se desmayó, para despertarse justo antes de que su oponente lo intentara atacar")
  }else if (elErudito.ang<14){
    damage = rollDice(10)
    elErudito.hpw= elErudito.hpw - damage
    console.log("El erudito grita 'TÚ ERES TONTO', esto lo delata  y " + activeHero.name + " le causa " + damage+ " puntos de daño al erudito")
  }else if (elErudito.ang<17){
    console.log(activeHero.name  + " Aprovecha un despiste y le pone las gafas del caos a su oponente")
      changeGlasses = true
    
  }else if (elErudito.ang<19){

    damage = rollDice(10)
    console.log("el erudito burla a " + activeHero.name + " y aprovecha el despiste causado para recuperar sus gafas")
    console.log(activeHero.name  + " golpea al erudito con un golpe lo suficientemente fuerte como para haberle podido causar " + damage+ " puntos de daño a un mortal")
    console.log("el erudito, con sus gafas puestas, no parece haber recibido ningún rasguño")

  }else if (elErudito.ang<21){
    
    console.log("el atacante desata toda su ira en el erudito, Y lo decapita")
    eruditeAlive = false
  }

  if (elErudito.hpw<1){
    eruditeAlive = false
    console.log("el erudito sufrió un golpe mortal")
  }

  return [eruditeAlive,changeGlasses]

}



