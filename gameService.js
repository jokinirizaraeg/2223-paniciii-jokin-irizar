export async  function getAllHeroes(){
    const response = await fetch('https://cdn.jsdelivr.net/gh/akabab/superhero-api@0.3.0/api/all.json');
    const allHeroes = await response.json();
    return allHeroes;
    }


    export async  function getJunkpile(){
        const response = await fetch('https://cdn.jsdelivr.net/gh/akabab/superhero-api@0.3.0/api/id/375.json');
        const junkpile = await response.json();
        return junkpile;
        }

        export async  function getHeroByID(id){
            const response = await fetch('https://cdn.jsdelivr.net/gh/akabab/superhero-api@0.3.0/api/id/'+id+'.json');
            const hero = await response.json();
            return hero;
            }
    

        export async  function getRandomHero(){

            const response = await fetch('https://cdn.jsdelivr.net/gh/akabab/superhero-api@0.3.0/api/all.json');
            const allHeroes = await response.json();
            let num = 375
            do {

                num = Math.floor(Math.random() * ((allHeroes.length-1) - 1 + 1)) + 1;
             } while (num==375)

            return allHeroes[num];
            }