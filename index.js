

import {calcIniciative, calcHP, combat} from './gameController.js';
import { getAllHeroes, getJunkpile, getRandomHero, getHeroByID} from './gameService.js';

async function fight () {

let hero = await getRandomHero()
let junkpile = await getJunkpile()

let heroIniciative = await calcIniciative(hero,junkpile)
let heroWithHP = await calcHP(hero)
let junkpileWithHP = await calcHP(junkpile)
combat(heroIniciative,heroWithHP,junkpileWithHP)


//console.log(heroIniciative)

}


fight()